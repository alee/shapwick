/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include <stdlib.h>
#include <mntent.h>
#include <sys/statvfs.h>
#include <glib.h>
#include <ftw.h>
#include <gio/gio.h>
#include "sw-private.h"
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <sys/inotify.h>

#define ROOT_MOUNT_SCHEMA "org.apertis.Ribchester.RootFsMntMgr"
#define VAR_MOUNT_SCHEMA "org.apertis.Ribchester.VarMntMgr"
#define HOME_MOUNT_SCHEMA "org.apertis.Ribchester.HomeMntMgr"
#define APPLICATIONS_MOUNT_SCHEMA "org.apertis.Ribchester.ApplicationsMntMgr"
#define GENERAL_MOUNT_SCHEMA  "org.apertis.Ribchester.GeneralMntMgr"

#define HOME_MOUNT_NAME "/home"

#define HOME_MUSIC_PATH      g_build_filename("/home",g_get_host_name (), "/Music", NULL)
#define HOME_VIDEOS_PATH      g_build_filename("/home",g_get_host_name (), "/Videos", NULL)
#define HOME_PICTURES_PATH    g_build_filename("/home",g_get_host_name (), "/Pictures", NULL)
#define HOME_DOWNLOADS_PATH    g_build_filename("/home",g_get_host_name (), "/Downloads", NULL)
#define GENERAL_MOUNT_NAME "/General"

#define VAR_MOUNT_NAME "/var"
#define VAR_LOG_PATH "/var/log"

#define ROOT_MOUNT_NAME "/rootfs"
#define APPLICATIONS_MOUNT_NAME "/Applications"
#define MONITOR_INTERVAL "monitor-interval"
#define TRIGGER_TRESHOLD "trigger-treshold"

GFileMonitor *pFileMonitor[10];

#define PERCENTAGE_USED_FSTAT(X) ( (100 * (double)(X->f_blocks - X->f_bavail)) / X->f_blocks)
#define PERCENTAGE_USED(X,Y) ((X/Y)* 100)

#define KB(X) ((X) / 1024)
#define MB(X) ((X) /(1024*1024))
#define GB(X) ((MB(X)) /(1024*1024))

typedef struct _ShapwickFsEntry ShapwickFsEntry;
struct _ShapwickFsEntry
{
  gdouble triggerTreshold;
  gdouble maximumSpace;
  gdouble percentUsed;
  guint timerID;
  gint monitorInterval;
  gchar *mountName;
  gchar *mountPath;
};

static GFileMonitor *pFileGeneralMonitor = NULL;
static GHashTable *fsResourceMgrHash = NULL;
static gint monitorIndex = 0;
static gboolean get_disk_usage_for_mount (ShapwickFileSystem * object,
                                          GDBusMethodInvocation * invocation,
                                          const gchar * mountName);
static void initialise_mount_data (const gchar * schemaName,
                                   const gchar * mountName, gpointer pObject);
static gboolean disk_usage_monitor_fn (ShapwickFsEntry * fsEntry);
static void rootfs_disk_usage_notifier (ShapwickFsEntry * fsEntry);
static void var_disk_usage_notifier (ShapwickFsEntry * fsEntry);
static void home_disk_usage_notifier (ShapwickFsEntry * fsEntry);
static void applications_disk_usage_notifier (ShapwickFsEntry * fsEntry);

void v_monitor_mount_paths (ShapwickFileSystem * object, gchar * pDirPath);
void callback (GFileMonitor * mon,
               GFile * first,
               GFile * second, GFileMonitorEvent event, gpointer udata);

static int total = 0;

static int
sum (const char *fpath, const struct stat *sb, int typeflag)
{

  struct stat statbuf;

  if (lstat (fpath, &statbuf))
    {
      perror (fpath);
    }
  else
    {
      if (S_ISLNK (statbuf.st_mode))
        {
          total = total + 8;
          return 0;
        }
    }
  //printf("%s %d Type Flag %d \n",fpath , ((int ) ( sb->st_blocks ))/2, typeflag);
  total = total + (int) (sb->st_blocks);
  return 0;
}

ShapwickFileSystem *FileSystemObject = NULL;

#if 0
Is there a case where we need to remove entries form the hash or clear the
  hash itslef ? static void filesw - private.hash_deinit (void);
static gboolean destroy_entry (gpointer key, gpointer value,
                               gpointer userData);
static void filesw - private.
hash_deinit (void)
{
  g_hash_table_foreach_remove (fsResourceMgrHash, (GHRFunc) destroy_entry,
                               NULL);

  g_hash_table_destroy (fsResourceMgrHash);

  fsResourceMgrHash = NULL;
}

static gboolean
destroy_entry (gpointer key, gpointer value, gpointer userData)
{
  if (NULL != key && NULL != value)
    {
      ShapwickFsEntry *fsData = (ShapwickFsEntry *) value;

      if (NULL != fsData)
        {
          /* Free memory held for the member variables */
          if (NULL != fsData->mountName)
            {
              g_free (fsData->mountName);
              fsData->mountName = NULL;
            }

          g_free (fsData);
          fsData = NULL;
        }
    }

  return TRUE;
}
#endif

static int
nodots (const struct dirent *dp)
{

  return (dp->d_name[0] != '.');
}

static gboolean
b_disconnect_disk_monitoring (ShapwickFileSystem * object,
                              GDBusMethodInvocation * invocation)
{
  gint i = 0;
  filesystem_debug ("%s\n\n", __FUNCTION__);

  for (i = 0; i < monitorIndex; i++)
    g_object_unref (pFileMonitor[i]);

  shapwick_file_system_complete_disconnect_disk_monitoring (object,
                                                                invocation);
  return TRUE;

}

static GVariant *
ftw_get_dir_size (const gchar * path)
{

  GVariantBuilder *gvb = NULL;
  GVariant *dict = NULL;

  if (ftw (path, &sum, 1) == 0)
    {
      // total = MB(total);
      gvb = g_variant_builder_new (G_VARIANT_TYPE ("a{sd}"));
      g_variant_builder_add (gvb, "{sd}", "Used", (double) total / 2);
      //   g_variant_builder_add (gvb, "{sd}", "Percentage", PERCENTAGE_USED(total, clientAppData->maximumSpace));
      dict = g_variant_builder_end (gvb);
      g_variant_builder_unref (gvb);
      filesystem_debug ("IN FTW FUNCTION values %s\n",
                        g_variant_print (dict, TRUE));
      //      filesystem_debug("Path %s Total %f Percentage %f\n", path, total, PERCENTAGE_USED(total, clientAppData->maximumSpace));

      total = 0;
    }

  return dict;
}

#ifndef ENABLE_UPDATE_ROLLBACK

static gboolean
b_disk_usage_status (ShapwickFileSystem * object,
                     GDBusMethodInvocation * invocation)
{
  GVariantBuilder *pGvb = NULL;
  GVariant *pValue = NULL;

  pGvb = g_variant_builder_new (G_VARIANT_TYPE ("a{sv}"));

  //v_monitor_mount_paths(object, APPLICATIONS_MOUNT_NAME);
  //v_monitor_mount_paths(object, VAR_LOG_PATH);
  v_monitor_mount_paths (object, HOME_MUSIC_PATH);
  v_monitor_mount_paths (object, HOME_DOWNLOADS_PATH);
  v_monitor_mount_paths (object, HOME_PICTURES_PATH);
  v_monitor_mount_paths (object, HOME_VIDEOS_PATH);

  //g_variant_builder_add ( pGvb, "{sv}", "APPLICATIONS", ftw_get_dir_size(APPLICATIONS_MOUNT_NAME));
  //g_variant_builder_add ( pGvb, "{sv}", "LOG", ftw_get_dir_size(VAR_LOG_PATH));
  g_variant_builder_add (pGvb, "{sv}", "MUSIC",
                         ftw_get_dir_size (HOME_MUSIC_PATH));
  g_variant_builder_add (pGvb, "{sv}", "VIDEOS",
                         ftw_get_dir_size (HOME_VIDEOS_PATH));
  g_variant_builder_add (pGvb, "{sv}", "PICTURES",
                         ftw_get_dir_size (HOME_PICTURES_PATH));
  g_variant_builder_add (pGvb, "{sv}", "DOWNLOADS",
                         ftw_get_dir_size (HOME_DOWNLOADS_PATH));

  pValue = g_variant_builder_end (pGvb);

  filesystem_debug ("Variant values %s\n", g_variant_print (pValue, TRUE));

  shapwick_file_system_emit_disk_usage_statistics (object, pValue);
  return TRUE;
}

static gboolean
b_check_disk_usage (ShapwickFileSystem * object, const gchar * mountName)
{

  if (ftw (mountName, &sum, 1) == 0)
    {

      ShapwickFsEntry *clientAppData =
        g_hash_table_lookup (fsResourceMgrHash, (gpointer) mountName);

      if (PERCENTAGE_USED (MB (total), clientAppData->maximumSpace) >
          clientAppData->triggerTreshold)
        {
          if (g_strcmp0 (mountName, "/var") == 0)
            {
              filesystem_debug
                ("Log Files Space is much higher.Please Clear the logs!\n");
              shapwick_file_system_emit_space_overflow_notification
                (object, "Not Enough Disk Space in Log Folder!");
            }

          else if (g_strrstr (mountName, HOME_MOUNT_NAME) != NULL)
            {
              filesystem_debug ("Home Folder OverFlow \n");
              shapwick_file_system_emit_space_overflow_notification
                (object, "Not Enough Disk Space in Home folder!");
            }
          else if (g_strcmp0 (mountName, GENERAL_MOUNT_NAME) == 0)
            {
              filesystem_debug ("General Folder OverFlow \n");
              shapwick_file_system_emit_space_overflow_notification
                (object, GENERAL_MOUNT_NAME);
            }
          else if (g_strcmp0 (mountName, APPLICATIONS_MOUNT_NAME) == 0)
            {
              filesystem_debug ("Applications Folder OverFlow \n");
              shapwick_file_system_emit_space_overflow_notification
                (object, "Not Enough Disk Space for Applications!");
            }
          else if (g_strcmp0 (mountName, "/rootfs") == 0)
            {
              /*Do Nothing */
            }

        }
    }

  return TRUE;
}

#else

static gboolean
b_disk_usage_status (ShapwickFileSystem * object,
                     GDBusMethodInvocation * invocation)
{

  GVariantBuilder *pGvb = NULL;
  GVariant *pValue = NULL;

  pGvb = g_variant_builder_new (G_VARIANT_TYPE ("a{sv}"));

  //FILE *fp = setmntent("temp_mount", "r");
  if (NULL != fp)
    {
      struct mntent *mountEntry;
      struct statvfs *fsStats = g_new0 (struct statvfs, 1);

      /* for each mount, get the mount specific data */
      while (NULL != (mountEntry = getmntent (fp)))
        {

          if (g_strcmp0 (mountEntry->mnt_fsname, APPLICATIONS_MOUNT_NAME) ==
              0)
            {

              v_monitor_mount_paths (object, APPLICATIONS_MOUNT_NAME);
              if (NULL != fsStats)
                {
                  if (statvfs (mountEntry->mnt_dir, fsStats) == 0)
                    {
                      filesystem_debug ("Percentage %f\n",
                                        PERCENTAGE_USED_FSTAT (fsStats));
                      g_variant_builder_add (pGvb, "{sv}", "APPLICATIONS",
                                             g_variant_new_double
                                             (PERCENTAGE_USED_FSTAT
                                              (fsStats)));
                    }
                }
            }

          if (g_strcmp0 (mountEntry->mnt_fsname, VAR_MOUNT_NAME) == 0)
            {

              if (NULL != fsStats)
                {
                  if (statvfs (mountEntry->mnt_dir, fsStats) == 0)
                    {
                      /* SIZE IN MB */
                      double avail =
                        (double) (fsStats->f_bsize * fsStats->f_bavail) /
                        (1024 * 1024);
                      double size =
                        (double) (fsStats->f_bsize * fsStats->f_blocks) /
                        (1024 * 1024);
                      filesystem_debug ("Percentage %f\n",
                                        (1 - (avail / size)) * 100);

                      v_monitor_mount_paths (object, VAR_LOG_PATH);
                      if (ftw (VAR_LOG_PATH, &sum, 1))
                        {
                          perror ("ftw");
                          //return 2;
                        }
                      g_variant_builder_add (pGvb, "{sv}", "LOG",
                                             g_variant_new_double ((MB (total)
                                                                    / size) *
                                                                   100));
                    }
                }
            }

          if (g_strcmp0 (mountEntry->mnt_fsname, HOME_MUSIC_PATH) == 0)
            {

              v_monitor_mount_paths (object, HOME_MUSIC_PATH);
              if (statvfs (mountEntry->mnt_dir, fsStats) == 0)
                {
                  g_variant_builder_add (pGvb, "{sv}", "MUSIC",
                                         g_variant_new_double
                                         (PERCENTAGE_USED_FSTAT (fsStats)));
                }
            }

          if (g_strcmp0 (mountEntry->mnt_fsname, HOME_VIDEOS_PATH) == 0)
            {

              v_monitor_mount_paths (object, HOME_VIDEOS_PATH);
              if (statvfs (mountEntry->mnt_dir, fsStats) == 0)
                {
                  g_variant_builder_add (pGvb, "{sv}", "VIDEOS",
                                         g_variant_new_double
                                         (PERCENTAGE_USED_FSTAT (fsStats)));
                }
            }

          if (g_strcmp0 (mountEntry->mnt_fsname, HOME_PICTURES_PATH) == 0)
            {

              v_monitor_mount_paths (object, HOME_PICTURES_PATH);
              if (statvfs (mountEntry->mnt_dir, fsStats) == 0)
                {
                  g_variant_builder_add (pGvb, "{sv}", "PICTURES",
                                         g_variant_new_double
                                         (PERCENTAGE_USED_FSTAT (fsStats)));
                }
            }

          if (g_strcmp0 (mountEntry->mnt_fsname, HOME_DOWNLOADS_PATH) == 0)
            {

              v_monitor_mount_paths (object, HOME_DOWNLOADS_PATH);
              if (statvfs (mountEntry->mnt_dir, fsStats) == 0)
                {
                  g_variant_builder_add (pGvb, "{sv}", "DOWNLOADS",
                                         g_variant_new_double
                                         (PERCENTAGE_USED_FSTAT (fsStats)));
                }
            }

          continue;

        }
      pValue = g_variant_builder_end (pGvb);
      g_free (fsStats);
    }
  else
    g_dbus_method_invocation_return_dbus_error (invocation,
                                                "Shapwick.FileSystem.Error.Failed",
                                                "Cannot read system mounts.");

  filesystem_debug ("Variant values %s\n", g_variant_print (pValue, TRUE));

  shapwick_file_system_emit_disk_usage_statistics (object, pValue);
  return TRUE;
}

gboolean
b_check_disk_usage (ShapwickFileSystem * object, const gchar * mountName)
{

  struct statvfs *fsStats = g_new0 (struct statvfs, 1);

  if (statvfs (mountName, fsStats) == 0)
    {

      ShapwickFsEntry *clientAppData =
        g_hash_table_lookup (fsResourceMgrHash, (gpointer) mountName);

      if (PERCENTAGE_USED_FSTAT (fsStats) > clientAppData->triggerTreshold)
        {

          if (g_strcmp0 (mountName, "/var") == 0)
            {
              filesystem_debug
                ("Log Files Space is much higher.Please Clear the logs!\n");
              shapwick_file_system_emit_space_overflow_notification
                (object, "Not Enough Disk Space in Log Folder!");
            }

          else if (g_strrstr (mountName, HOME_MOUNT_NAME) != NULL)
            {
              filesystem_debug ("Home Folder OverFlow \n");
              shapwick_file_system_emit_space_overflow_notification
                (object, "Not Enough Disk Space in Home folder!");
            }

          else if (g_strcmp0 (mountName, APPLICATIONS_MOUNT_NAME) == 0)
            {
              filesystem_debug ("Applications Folder OverFlow \n");
              shapwick_file_system_emit_space_overflow_notification
                (object, "Not Enough Disk Space for Applications!");
            }
          else if (g_strcmp0 (mountName, "/rootfs") == 0)
            {
              /*Do Nothing */
            }

        }
    }
  g_free (fsStats);
  return TRUE;
}

#endif

void
v_monitor_mount_paths (ShapwickFileSystem * object, gchar * pDirPath)
{

  GFile *file;

  file = g_file_new_for_path (pDirPath);
  g_assert (file != NULL);

  pFileMonitor[monitorIndex] =
    g_file_monitor (file, G_FILE_MONITOR_SEND_MOVED, NULL, NULL);
  g_assert (pFileMonitor[monitorIndex] != NULL);

  g_signal_connect (pFileMonitor[monitorIndex], "changed",
                    G_CALLBACK (callback), object);

  monitorIndex = monitorIndex + 1;

}

static gboolean
get_disk_usage_for_mount (ShapwickFileSystem * object,
                          GDBusMethodInvocation * invocation,
                          const gchar * mountName)
{
  filesystem_debug ("%s\n", __FUNCTION__);
  if (NULL != mountName)
    {
      if (b_check_disk_usage (object, mountName))
        {
          filesystem_debug ("Disk usage check successful\n");
        }
      else
        g_dbus_method_invocation_return_dbus_error (invocation,
                                                    "Shapwick.FileSystem.Error.Failed",
                                                    "Cannot read system mounts.");
    }
  else
    g_dbus_method_invocation_return_dbus_error (invocation,
                                                "Shapwick.FileSystem.Error.Failed",
                                                "Mount point not monitored or incorrect mount name provided");

  return TRUE;
}

static void
applications_disk_usage_notifier (ShapwickFsEntry * fsEntry)
{
  if (b_check_disk_usage (FileSystemObject, APPLICATIONS_MOUNT_NAME))
    {
      filesystem_debug ("Disk usage call for Applications-> Success\n");
    }
  else
    filesystem_debug ("Disk usage call for Applications->Failed\n");
}

static void
home_disk_usage_notifier (ShapwickFsEntry * fsEntry)
{
  if (b_check_disk_usage (FileSystemObject, fsEntry->mountName))
    {
      filesystem_debug ("Disk usage call for Home-> Success\n");
    }
  else
    filesystem_debug ("Disk usage call for Home->Failed\n");
}

static void
var_disk_usage_notifier (ShapwickFsEntry * fsEntry)
{
  if (b_check_disk_usage (FileSystemObject, "/var"))
    {
      filesystem_debug ("Disk usage call for Var-> Success\n");
    }
  else
    filesystem_debug ("Disk usage call for Var->Failed\n");
}

static void
rootfs_disk_usage_notifier (ShapwickFsEntry * fsEntry)
{
  if (b_check_disk_usage (FileSystemObject, "rootfs"))
    {
      filesystem_debug ("Disk usage call for RootFS-> Success\n");
    }
  else
    filesystem_debug ("Disk usage call for RootFS->Failed\n");
}

static void
general_disk_usage_notifier (ShapwickFsEntry * fsEntry)
{
  if (b_check_disk_usage (FileSystemObject, GENERAL_MOUNT_NAME))
    {
      filesystem_debug ("Disk usage call for RootFS-> Success\n");
    }
  else
    filesystem_debug ("Disk usage call for RootFS->Failed\n");
}

static gboolean
disk_usage_monitor_fn (ShapwickFsEntry * fsEntry)
{
  filesystem_debug ("%s for %s\n ", __FUNCTION__, fsEntry->mountName);
  if (NULL != fsEntry)
    {
      /* Check the timer. based on mount for which the timer expiry has occured, take appropriate actions */
      if (g_strcmp0 (fsEntry->mountName, ROOT_MOUNT_NAME) == 0)
        rootfs_disk_usage_notifier (fsEntry);

      if (g_strcmp0 (fsEntry->mountName, VAR_MOUNT_NAME) == 0)
        var_disk_usage_notifier (fsEntry);

      if (g_strcmp0 (fsEntry->mountName, HOME_MUSIC_PATH) == 0 ||
          g_strcmp0 (fsEntry->mountName, HOME_PICTURES_PATH) == 0 ||
          g_strcmp0 (fsEntry->mountName, HOME_VIDEOS_PATH) == 0 ||
          g_strcmp0 (fsEntry->mountName, HOME_DOWNLOADS_PATH) == 0)
        home_disk_usage_notifier (fsEntry);

      if (g_strcmp0 (fsEntry->mountName, APPLICATIONS_MOUNT_NAME) == 0)
        applications_disk_usage_notifier (fsEntry);
      if (g_strcmp0 (fsEntry->mountName, GENERAL_MOUNT_NAME) == 0)
        general_disk_usage_notifier (fsEntry);
    }
  return TRUE;
}

static void
initialise_mount_data (const gchar * schemaName, const gchar * mountName,
                       gpointer pObject)
{
  if ((NULL != schemaName) && (NULL != mountName) &&
      (NULL != fsResourceMgrHash))
    {
      GSettings *pSchema = g_settings_new (schemaName);

      /*Read the schema */
      if (NULL != pSchema)
        {
          /* Allocate a new fsEntry struct instance */
          ShapwickFsEntry *fsEntry = g_new0 (ShapwickFsEntry, 1);

          if (NULL != fsEntry)
            {
              /* Copy all fs settings */
              fsEntry->mountPath =
                g_settings_get_string (pSchema, "mount-path");
              fsEntry->monitorInterval =
                g_settings_get_uint (pSchema, "monitor-interval");
              fsEntry->triggerTreshold =
                g_settings_get_uint (pSchema, "trigger-treshold");
              fsEntry->maximumSpace =
                g_settings_get_uint (pSchema, "maximum-space");
              fsEntry->mountName = g_strdup (mountName);

              if (g_strcmp0 (fsEntry->mountName, GENERAL_MOUNT_NAME) == 0)
                shapwick_file_system_set_application_subvolume_max_size
                  (SHAPWICK_FILE_SYSTEM (pObject),
                   g_settings_get_uint (pSchema,
                                        "application-subvolume-max-size"));
              /* Add to hash */
              g_hash_table_replace (fsResourceMgrHash, g_strdup (mountName),
                                    (gpointer) fsEntry);
              filesystem_debug (" Mount name %s Timer %d \n",
                                fsEntry->mountName, fsEntry->monitorInterval);
              /*Setup the monitor function */
              fsEntry->timerID =
                g_timeout_add (fsEntry->monitorInterval,
                               (GSourceFunc) disk_usage_monitor_fn, fsEntry);
            }
        }
    }
}

static char *
decode (GFileMonitorEvent ev)
{
  char *fmt = g_malloc0 (1024);
  int caret = 0;

#define dc(x)  \
    case G_FILE_MONITOR_EVENT_##x: \
        strcat(fmt, #x); \
        caret += strlen(#x); \
        fmt[caret] = ':'; \
        break;

  switch (ev)
    {
      dc (CHANGED);
      dc (CHANGES_DONE_HINT);
      dc (DELETED);
      dc (CREATED);
      dc (ATTRIBUTE_CHANGED);
      dc (PRE_UNMOUNT);
      dc (UNMOUNTED);
      dc (MOVED);
      /* FIXME: unintended missing cases? */
      case G_FILE_MONITOR_EVENT_RENAMED:
      case G_FILE_MONITOR_EVENT_MOVED_IN:
      case G_FILE_MONITOR_EVENT_MOVED_OUT:
      default:
        break;
    }
#undef dc

  return fmt;
}

void
callback (GFileMonitor * mon,
          GFile * first,
          GFile * second, GFileMonitorEvent event, gpointer object)
{
  char *msg = decode (event);

  //FILE *fp = setmntent("/proc/mounts", "r");
  struct statvfs *fsStats = g_new0 (struct statvfs, 1);

#define fn(x) ((x) ? g_file_get_basename (x) : "--")
  filesystem_debug
    ("Received event %s (code %d), first file \"%s\", second file \"%s\"\n",
     msg, event, fn (first), fn (second));
#undef fn

  filesystem_debug ("The path %s \n",
                    g_file_get_path (g_file_get_parent (first)));

  if (g_strcmp0
      (g_file_get_path (g_file_get_parent (first)), GENERAL_MOUNT_NAME) == 0)
    {
      ShapwickFsEntry *clientAppData = NULL;

      if (statvfs (g_file_get_path (g_file_get_parent (first)), fsStats) == 0)
        {
          filesystem_debug
            ("statvfs call inside monitor call back success!!\n");
        }

      clientAppData =
        g_hash_table_lookup (fsResourceMgrHash,
                             (gpointer) GENERAL_MOUNT_NAME);

      if (PERCENTAGE_USED_FSTAT (fsStats) > clientAppData->triggerTreshold)
        {

          shapwick_file_system_emit_space_overflow_notification ((ShapwickFileSystem *) object, "Space overflow in General Folder!");
        }

    }

  if (g_strcmp0
      (g_file_get_path (g_file_get_parent (first)),
       APPLICATIONS_MOUNT_NAME) == 0)
    {
      shapwick_file_system_emit_disk_usage_notification ((ShapwickFileSystem *) object, "APPLICATIONS", ftw_get_dir_size (APPLICATIONS_MOUNT_NAME));
    }
  if (g_strcmp0 (g_file_get_path (g_file_get_parent (first)), VAR_LOG_PATH) ==
      0)
    {
      shapwick_file_system_emit_disk_usage_notification ((ShapwickFileSystem *) object, "LOG", ftw_get_dir_size (VAR_LOG_PATH));
    }
  if (g_strcmp0 (g_file_get_path (g_file_get_parent (first)), HOME_MUSIC_PATH)
      == 0)
    {
      shapwick_file_system_emit_disk_usage_notification ((ShapwickFileSystem *) object, "MUSIC", ftw_get_dir_size (HOME_MUSIC_PATH));
    }
  if (g_strcmp0
      (g_file_get_path (g_file_get_parent (first)), HOME_DOWNLOADS_PATH) == 0)
    {
      shapwick_file_system_emit_disk_usage_notification ((ShapwickFileSystem *) object, "DOWNLOADS", ftw_get_dir_size (HOME_DOWNLOADS_PATH));
    }
  if (g_strcmp0
      (g_file_get_path (g_file_get_parent (first)), HOME_VIDEOS_PATH) == 0)
    {
      shapwick_file_system_emit_disk_usage_notification ((ShapwickFileSystem *) object, "VIDEOS", ftw_get_dir_size (HOME_VIDEOS_PATH));
    }
  if (g_strcmp0
      (g_file_get_path (g_file_get_parent (first)), HOME_PICTURES_PATH) == 0)
    {
      shapwick_file_system_emit_disk_usage_notification ((ShapwickFileSystem *) object, "PICTURES", ftw_get_dir_size (HOME_PICTURES_PATH));
    }
  g_free (msg);
  g_free (fsStats);
}

static void
v_get_app_install_info (ShapwickFileSystem * object,
                        GDBusMethodInvocation * invocation,
                        gdouble arg_app_size)
{
  GVariantBuilder *gvb = NULL;
  GVariant *dict = NULL;
  FILE *fp = NULL;
  GFile *file = NULL;

  gvb = g_variant_builder_new (G_VARIANT_TYPE ("a{sv}"));

  fp = setmntent ("/proc/mounts", "r");

  if (NULL != fp)
    {
      struct mntent *mountEntry;
      struct statvfs *fsStats = g_new0 (struct statvfs, 1);

      /* for each mount, get the mount specific data */

      while (NULL != (mountEntry = getmntent (fp)))
        {

          ShapwickFsEntry *clientAppData =
            g_hash_table_lookup (fsResourceMgrHash,
                                 (gpointer) mountEntry->mnt_dir);
          if (clientAppData != NULL)
            {

              if (NULL != fsStats)
                {
                  if (statvfs (mountEntry->mnt_dir, fsStats) == 0)
                    {
                      //g_print("General Space %ld\n", MB(fsStats->f_bavail* fsStats->f_bsize));
                      GVariant *info = NULL;
                      GVariantBuilder *gvb1 =
                        g_variant_builder_new (G_VARIANT_TYPE ("a{sd}"));
                      g_variant_builder_add (gvb1, "{sd}",
                                             g_strdup ("AvailableSpace"),
                                             (gdouble) (KB
                                                        (fsStats->f_bavail *
                                                         fsStats->f_bsize)));
                      g_variant_builder_add (gvb1, "{sd}", g_strdup ("Total"),
                                             (gdouble) (fsStats->f_blocks *
                                                        4));
                      g_variant_builder_add (gvb1, "{sd}",
                                             g_strdup ("Threshold"),
                                             clientAppData->triggerTreshold);
                      info = g_variant_builder_end (gvb1);

                      g_variant_builder_add (gvb, "{sv}", mountEntry->mnt_dir,
                                             info);

                    }
                }

            }

        }
      g_free (fsStats);
      dict = g_variant_builder_end (gvb);
    }
  endmntent (fp);
  filesystem_debug ("App installation values %s\n",
                    g_variant_print (dict, TRUE));

#if 1
  file = g_file_new_for_path (GENERAL_MOUNT_NAME);
  g_assert (file != NULL);

  pFileGeneralMonitor =
    g_file_monitor (file, G_FILE_MONITOR_SEND_MOVED, NULL, NULL);
  g_assert (pFileGeneralMonitor != NULL);

  g_signal_connect (pFileGeneralMonitor, "changed", G_CALLBACK (callback),
                    object);

#endif
  shapwick_file_system_complete_get_app_install_info (object, invocation,
                                                          dict);

}

static void
v_get_app_size_info (ShapwickFileSystem * object,
                     GDBusMethodInvocation * invocation,
                     const gchar * arg_path_name)
{
  DIR *dp;
  GVariant *info = NULL;
  GVariantBuilder *gvb = NULL;
  struct dirent *entry;
  struct stat statbuf;

  if ((dp = opendir (arg_path_name)) == NULL)
    {
      fprintf (stderr, "cannot open directory: %s\n", arg_path_name);
      gvb = g_variant_builder_new (G_VARIANT_TYPE ("a{sd}"));

      info = g_variant_builder_end (gvb);
      shapwick_file_system_complete_get_app_size_info (object, invocation,
                                                           info);
      return;
    }
  if (0 != chdir (arg_path_name))
    g_message ("Unable to find directory:%s\n", arg_path_name);

  gvb = g_variant_builder_new (G_VARIANT_TYPE ("a{sd}"));

  while ((entry = readdir (dp)) != NULL)
    {
      lstat (entry->d_name, &statbuf);
      if (S_ISDIR (statbuf.st_mode))
        {
          gchar *path = NULL;
          /* Found a directory, but ignore . and .. */
          if (strcmp (".", entry->d_name) == 0 ||
              strcmp ("..", entry->d_name) == 0)
            continue;
          path =
            g_strconcat (arg_path_name, "/", entry->d_name, "/", NULL);
          g_print ("Path %s \n", path);

          if (ftw (path, &sum, 1) == 0)
            {
              //printf("%s %d\n",entry->d_name , total/2);
              g_variant_builder_add (gvb, "{sd}", g_strdup (path),
                                     (double) total / 2);
              total = 0;
            }
        }
    }
  if (0 != chdir (".."))
    g_message ("Somthing went wrong navigating to parent dir\n");

  closedir (dp);
  info = g_variant_builder_end (gvb);
  shapwick_file_system_complete_get_app_size_info (object, invocation,
                                                       info);
}

static void
v_app_install_done (ShapwickFileSystem * object,
                    GDBusMethodInvocation * invocation)
{

  filesystem_debug ("%s\n", __FUNCTION__);
  shapwick_file_system_complete_app_install_done (object, invocation);
  g_object_unref (pFileGeneralMonitor);
}

void
fileshapwick_init (ShapwickFileSystem * object)
{
  filesystem_debug ("%s\n", __FUNCTION__);
  FileSystemObject = object;
  fsResourceMgrHash = g_hash_table_new (g_str_hash, g_str_equal);

  if (NULL != fsResourceMgrHash)
    {
      initialise_mount_data (ROOT_MOUNT_SCHEMA, ROOT_MOUNT_NAME, object);
      initialise_mount_data (HOME_MOUNT_SCHEMA, HOME_MUSIC_PATH, object);
      initialise_mount_data (HOME_MOUNT_SCHEMA, HOME_PICTURES_PATH, object);
      initialise_mount_data (HOME_MOUNT_SCHEMA, HOME_VIDEOS_PATH, object);
      initialise_mount_data (HOME_MOUNT_SCHEMA, HOME_DOWNLOADS_PATH, object);
      initialise_mount_data (APPLICATIONS_MOUNT_SCHEMA,
                             APPLICATIONS_MOUNT_NAME, object);
      initialise_mount_data (GENERAL_MOUNT_SCHEMA, GENERAL_MOUNT_NAME,
                             object);
      //initialize_popup_client_handler();
      /* provide signal handlers for all methods */

      /* The size of Dir where Apps are kept with its threshold values are computed and
       * send back to Application to check whether App can be installed*/
      g_signal_connect (object,
                        "handle-get-app-install-info",
                        G_CALLBACK (v_get_app_install_info), NULL);
      /* The List of installed Apps and their corresponding size */
      g_signal_connect (object,
                        "handle-get-app-size-info",
                        G_CALLBACK (v_get_app_size_info), NULL);
      /*The method to stop monitoring once application installations is done */
      g_signal_connect (object,
                        "handle-app-install-done",
                        G_CALLBACK (v_app_install_done), NULL);
      /*The method to Know the disk usage for that particular mount directory */
      g_signal_connect (object,
                        "handle-get-disk-usage-for-mount",
                        G_CALLBACK (get_disk_usage_for_mount), NULL);
      /*The method to start monitoring once System Monitoring app is launched */
      g_signal_connect (object,
                        "handle-get-disk-usage-statistics",
                        G_CALLBACK (b_disk_usage_status), NULL);
      /*The method to stop monitoring once System Monitoring app is exited */
      g_signal_connect (object,
                        "handle-disconnect-disk-monitoring",
                        G_CALLBACK (b_disconnect_disk_monitoring), NULL);

    }

}
