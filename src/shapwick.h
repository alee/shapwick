/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef  __SHAPWICK_H__
#define  __SHAPWICK_H__
#include "org.apertis.Shapwick.h"

enum _ShapwickBandwidthPrio
{
	SHAPWICK_HIGHEST,
	SHAPWICK_HIGH,
	SHAPWICK_MID,
	SHAPWICK_LOW,
	SHAPWICK_LOWEST,
	SHAPWICK_UNKNOWN
};
typedef enum _ShapwickBandwidthPrio ShapwickBandwidthPrio;

#endif /* __SHAPWICK_H__ */
