/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __SHAPWICK_PRIVATE_H__
#define __SHAPWICK_PRIVATE_H__

#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <gio/gio.h>
#include <string.h>
#include "shapwick.h"

#define CGROUP_SUBSYSTEM_MEMORY             "memory"
#define CGROUP_SUBGROUP_MEMORY              "Application"
#define CGROUP_MEMORY_MOVE_CHARGE           "memory.move_charge_at_immigrate"

#define CGROUP_BANDWIDTH_PRIO_MAP	          "net_prio.ifpriomap"
#define CGROUP_SUBSYSTEM_BANDWIDTH          "net_prio"
#define CGROUP_SUBGROUP_BANDWIDTH_LOWEST    "lowest_prio"
#define CGROUP_SUBGROUP_BANDWIDTH_LOW       "low_prio"
#define CGROUP_SUBGROUP_BANDWIDTH_MID       "mid_prio"
#define CGROUP_SUBGROUP_BANDWIDTH_HIGH      "high_prio"
#define CGROUP_SUBGROUP_BANDWIDTH_HIGHEST   "highest_prio"

#define CGROUP_SUBSYSTEM_CPU                "cpu"

typedef struct _ShapwickObjStrct ShapwickStrct;

struct _ShapwickObjStrct
{
  gchar *pMemoryCntrlrMountPoint;
  struct cgroup *pMemoryCgroup;
  ShapwickMemory *pMemoryObject;
  ShapwickFileSystem *pFileSystemObject;
  gchar *pBandwidthCntrlrMountPoint;
  struct cgroup *lowest_prio_cgroup;
  struct cgroup *low_prio_cgroup;
  struct cgroup *mid_prio_cgroup;
  struct cgroup *high_prio_cgroup;
  struct cgroup *highest_prio_cgroup;
  ShapwickBandwidth *pBandwidthObject;

  gchar *pCPUCntrlrMountPoint;
};

gpointer memory_resource_mgr_thread (gpointer data);
gboolean is_cgroup_subsystem_enabled (const char *name);
void register_to_net_connman (void);
void fileshapwick_init (ShapwickFileSystem * object);

//#define RES_MGR_DEBUG
#define BANDWIDTH_DEBUG
#define MEMORY_DEBUG
#define FILESYSTEM_DEBUG
#ifdef RES_MGR_DEBUG
#define res_mgr_debug( a ...) \
    {			            	\
        g_print("RESMGR:: "a);		\
    }

#else
#define res_mgr_debug( a ...)

#endif /*RES_MGR_DEBUG */

#ifdef BANDWIDTH_DEBUG
#define bandwidth_debug( a ...) \
    {			            	\
        g_print("BWDTH:: "a);		\
    }

#else
#define bandwidth_debug( a ...)

#endif /*BANDWIDTH_DEBUG */

#ifdef MEMORY_DEBUG
#define memory_debug( a ...) \
    {			            	\
        g_print("mem:: "a);		\
    }

#else
#define memory_debug( a ...)

#endif /*MEMORY_DEBUG */

#ifdef FILESYSTEM_DEBUG
#define filesystem_debug( a ...) \
    {			            	\
        g_print("FSYSTEM:: "a);		\
    }

#else
#define filesystem_debug( a ...)

#endif /*FILESYSTEM_DEBUG */

#endif /* __SHAPWICK_PRIVATE_H__ */
