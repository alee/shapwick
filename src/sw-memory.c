/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* vim: set sts=2 sw=2 et :
 *
 * This program listens for cgroup events for memory, and prints an event when
 * the given threshold is crossed in either direction.
 *
 * It doesn't do error checking
 *
 */
#define _GNU_SOURCE
#include <sys/eventfd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include "sw-private.h"

#define handle_error(msg) \
  do { perror(msg); exit(EXIT_FAILURE); } while (0)

#define MEMORY_CGROUP_PATH	    "/sys/fs/cgroup/memory/"

#define MEMORY_CGROUP_NAME       "Application"
#define MEMORY_CGROUP_THRESHOLD  "550000000"    //550MB

extern ShapwickStrct ShapwickObj;

static int
get_memory_usage_file_fd (const gchar * pCgroupName)
{
  gint ret;
  gint inMemoryUsageFd;
  gchar *pMemoryUsageFile;

  ret = asprintf (&pMemoryUsageFile,
                  MEMORY_CGROUP_PATH "%s/memory.usage_in_bytes", pCgroupName);
  if (ret == -1)
    handle_error ("asprintf");

  inMemoryUsageFd = open (pMemoryUsageFile, O_RDONLY);
  if (inMemoryUsageFd == -1)
    handle_error ("open");

  g_free (pMemoryUsageFile);
  return inMemoryUsageFd;
}

static void
subscribe_to_cgroup_events (gint inListenerFd,
                            gint inMemoryUsageFd,
                            const gchar * pMemoryThreshold,
                            const gchar * pMemoryCgroupName)
{
  gchar *pEventControlFile;
  gchar *line;
  gint inEventControlFd;
  gint ret;

  ret = asprintf (&pEventControlFile,
                  MEMORY_CGROUP_PATH "%s/cgroup.event_control",
                  pMemoryCgroupName);
  if (ret == -1)
    handle_error ("asprintf");

  inEventControlFd = open (pEventControlFile, O_WRONLY);
  if (inEventControlFd == -1)
    handle_error ("open");

  /* Generate the string to be written for subscribing to notifications */
  ret = asprintf (&line, "%d %d %s",
                  inListenerFd, inMemoryUsageFd, pMemoryThreshold);
  if (ret == -1)
    handle_error ("asprintf");

  /* Subscribe to memory usage threshold notifications */
  if (write (inEventControlFd, line, strlen (line) + 1) == -1)
    handle_error ("write");

  g_free (pEventControlFile);
  g_free (line);
}

gpointer
memory_resource_mgr_thread (gpointer data)
{
  gint ret;
  gint inListenerFd;
  const gchar *pMemoryThreshold;
  const gchar *pMemoryCgroupName;
  guint64 event;

  pMemoryCgroupName = MEMORY_CGROUP_NAME;
  pMemoryThreshold = MEMORY_CGROUP_THRESHOLD;

  inListenerFd = eventfd (0, 0);
  if (inListenerFd == -1)
    handle_error ("eventfd");

  subscribe_to_cgroup_events (inListenerFd,
                              get_memory_usage_file_fd (pMemoryCgroupName),
                              pMemoryThreshold, pMemoryCgroupName);

  memory_debug ("Waiting for memory threshold notification...\n");
  //blocking call
  ret = read (inListenerFd, &event, sizeof (event));
  if (ret == -1)
    handle_error ("read");

  g_print ("Threshold crossed!\n");
  shapwick_memory_emit_oomnotification (ShapwickObj.pMemoryObject);

  return data;
}
